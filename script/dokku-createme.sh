#!/bin/bash

# Change this to something unique for dokku
export DOKKU_APP_NAME="locomotivecms"

# Change this to your dokku command
export DOKKU="dokku-demo"

# Change this to your dokku user
export DOKKU_USER="dokku"

# Change this to your host
export DOKKU_HOST="demo.isula.net"

export MONGO_CONFIG_OPTIONS=" --auth "
export MONGO_IMAGE_VERSION="2.6.11"

# Change to what you want your mongo instance named
export MONGO_APP_NAME="mongo"

$DOKKU plugin:install https://github.com/dokku/dokku-mongo.git mongo
$DOKKU mongo:create $MONGO_APP_NAME

git clone git@gitlab.com:Isula/boilerplate-dokku-locomotivecms-engine.git $DOKKU_APP_NAME

cd $DOKKU_APP_NAME

git remote add demo $DOKKU_USER@$DOKKU_HOST:$DOKKU_APP_NAME
git push demo master

$DOKKU mongo:link $MONGO_APP_NAME $DOKKU_APP_NAME
$DOKKU config:set MONGODB_DATABASE=$DOKKU_APP_NAME
$DOKKU ps:restart $DOKKU_APP_NAME

