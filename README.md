# Isula / boilerplate-dokku-locomotivecms-engine
This boilerplate is what we use to deploy locomotiveCMS.

# Requirements
  - MongoDB v2.6 (https://github.com/dokku/dokku-mongo)
  - Running Dokku server
  - Linked containers

# Deploy 
```bash
#
# ~/.bashrc
# function dokku-demo {
#	ssh -t dokku@demo.isula.net $@
# }
#

# Change this to something unique for dokku
DOKKU_APP_NAME="locomotivecms"

# Change this to your dokku command
DOKKU="dokku-demo"

# Change this to your dokku user
DOKKU_USER="dokku"

# Change this to your host
DOKKU_HOST="demo.isula.net"

MONGO_CONFIG_OPTIONS=" --auth "
MONGO_IMAGE_VERSION="2.6.11"

# Change to what you want your mongo instance named
MONGO_APP_NAME="mongo"

$DOKKU plugin:install https://github.com/dokku/dokku-mongo.git mongo
$DOKKU mongo:create $MONGO_APP_NAME

git clone git@gitlab.com:Isula/boilerplate-dokku-locomotivecms-engine.git $DOKKU_APP_NAME

cd $DOKKU_APP_NAME

git remote add demo $DOKKU_USER@$DOKKU_HOST:$DOKKU_APP_NAME

$DOKKU apps:create $DOKKU_APP_NAME
$DOKKU mongo:link $MONGO_APP_NAME $DOKKU_APP_NAME
$DOKKU config:set $DOKKU_APP_NAME MONGODB_DATABASE=$DOKKU_APP_NAME

git push demo master
```



